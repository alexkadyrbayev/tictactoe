using System.Collections.Generic;

public interface IFieldPresenter
{
    public void Initialize();
    public void RegisterCell(TicTacCell cell);
    public void ClearTicTacData();
    public bool IsLoadedFieldEmpty();
    public List<TicTacCellModel> LoadedCells { get; }
}