using System;
using System.Collections.Generic;
using AlexKadyrbayev.Plugins.DataHandler;
using AlexKadyrbayev.Plugins.DataManagement;
using ModestTree;
using UnityEngine;
using Zenject;

public sealed class FieldPresenter : IFieldPresenter
{
    [Inject] private IDataHandlersRegistry m_DataHandlersRegistry;
    [Inject] private IPlayerInputController m_PlayerInputController;

    private Transform m_CellsParent;
    private IDataHandler<TicTacToeData> m_TicTacToeDataHandler;

    public List<TicTacCellModel> LoadedCells => m_TicTacToeDataHandler?.Data.TicTacCellModels;

    public void Initialize()
    {
        m_PlayerInputController.Initialize();
        LoadDataHandlers();
    }

    private void LoadDataHandlers()
    {
        m_TicTacToeDataHandler = m_DataHandlersRegistry.GetDataHandlerObject<TicTacToeData>("tic_tac_toe_data");
    }

    public void RegisterCell(TicTacCell cell)
    {
        m_TicTacToeDataHandler.Data.RegisterTicTacCellModel(
            new TicTacCellModel(cell.PositionInfo,
                cell.CellType.ToString()));
        
        m_TicTacToeDataHandler.Save();
    }

    public void ClearTicTacData()
    {
        m_TicTacToeDataHandler.Data.ClearCellModels();
        
        m_TicTacToeDataHandler.Save();
    }

    public bool IsLoadedFieldEmpty()
    {
        return m_TicTacToeDataHandler.Data.TicTacCellModels == null
               || m_TicTacToeDataHandler.Data.TicTacCellModels.Count == 0;
    }
}