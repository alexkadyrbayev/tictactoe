public interface IFieldModel
{
    public int X { get; }
    public int Y { get; }
}
