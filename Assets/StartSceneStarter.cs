using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartSceneStarter : MonoBehaviour
{
    [SerializeField] private Button m_PlayButton;

    private void Start()
    {
        m_PlayButton.onClick.AddListener(OnButtonClicked);
    }

    private void OnButtonClicked()
    {
        SceneManager.LoadScene("CoreScene");
    }
}
