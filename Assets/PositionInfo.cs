using System;
using Newtonsoft.Json;
using UnityEngine;

[Serializable, JsonObject(MemberSerialization.Fields)]
public class PositionInfo
{
    [SerializeField] private int m_X;
    [SerializeField] private int m_Y;

    public int X => m_X;
    public int Y => m_Y;

    public PositionInfo(int x, int y)
    {
        m_X = x;
        m_Y = y;
    }
}
