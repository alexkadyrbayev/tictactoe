using System;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class FieldView : MonoBehaviour, IFieldView
{
    [SerializeField] private Text m_WinnerText;
    [SerializeField] private Button m_RestartButton;
    [SerializeField] private Image m_Background;

    [Inject] private IFieldPresenter m_FieldPresenter;
    [Inject] private IFieldModel m_FieldModel;
    [Inject] private TicTacCell[,] m_Cells;
    [Inject] private IPlayerInputController m_PlayerInputController;
    
    private int m_FieldSizeX;
    private int m_FieldSizeY;

    public void Start()
    {
        m_FieldPresenter.Initialize();
        
        m_FieldSizeX = m_FieldModel.X;
        m_FieldSizeY = m_FieldModel.Y;
        
        SetCellsPosition();
        SetCameraPosition();
        
        LoadFieldIfNeeded();

        m_PlayerInputController.OnGameCompleted += OnGameCompleted;
        
        m_RestartButton.onClick.AddListener(OnRestartButtonClicked);
    }

    private void OnRestartButtonClicked()
    {
        m_FieldPresenter.ClearTicTacData();
        SceneManager.LoadScene("CoreScene");
    }
    
    private void SetCameraPosition()
    {
        var camera = Camera.main;
        var midCellPosition = m_Cells[m_FieldSizeX / 2, m_FieldSizeY / 2].transform.position;
        camera.transform.position = new Vector3(midCellPosition.x, midCellPosition.y, -10);
    }
    
    private void SetCellsPosition()
    {
        for (int row = 0; row < m_FieldSizeX; row++)
        {
            for (int col = 0; col < m_FieldSizeY; col++)
            {
                var position = new Vector3(col * 2, -row * 2, 0);
                
                var cell = m_Cells[row, col];
                cell.transform.position = position;
            }
        }
    }

    private void LoadFieldIfNeeded()
    {
        if (m_FieldPresenter.IsLoadedFieldEmpty())
        {
            return;
        }

        foreach (var cell in m_FieldPresenter.LoadedCells)
        {
            var cellType = (CellType)Enum.Parse(typeof(CellType), cell.ObjectName);

            var fieldCell = m_Cells[cell.PositionInfo.X, cell.PositionInfo.Y];
            fieldCell.SetCellType(cellType);
            fieldCell.LockTap();
            fieldCell.SetIcon(m_PlayerInputController.CellSpriteMap[cellType]);
        }

        var lastCellType = (CellType)Enum.Parse(typeof(CellType), 
            m_FieldPresenter.LoadedCells.Last().ObjectName);

        var currentPlayer = lastCellType == CellType.Cross ? PlayerType.Two : PlayerType.One;
        
        m_PlayerInputController.SetCurrentPlayerIfNeeded(currentPlayer);
    }

    private void OnGameCompleted(PlayerType type)
    {
        m_FieldPresenter.ClearTicTacData();
        
        m_Background.gameObject.SetActive(true);
        m_WinnerText.gameObject.SetActive(true);
        m_RestartButton.gameObject.SetActive(true);

        m_WinnerText.text = type == PlayerType.None ? "It's a draw!" : $"Winner is player: {type}";
    }
}