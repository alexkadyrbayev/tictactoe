using System;
using System.Collections.Generic;
using System.Linq;
using Services;
using UnityEngine;
using Zenject;
using Zenject.SpaceFighter;

public sealed class PlayerInputController : IPlayerInputController, IDisposable
{
    [Inject] private TicTacCell[,] m_Cells;
    [Inject] private IAssetManager m_AssetManager;
    [Inject] private IFieldModel m_FieldModel;
    [Inject] private IFieldPresenter m_FieldPresenter;

    private PlayerType m_CurrentPlayer = PlayerType.One;

    private Dictionary<PlayerType, CellType> m_PlayerCellTypeMap =
        new Dictionary<PlayerType, CellType>()
        {
            { PlayerType.One, CellType.Cross },
            { PlayerType.Two, CellType.Circle }
        };

    private Dictionary<CellType, Sprite> m_CellSpriteMap =
        new Dictionary<CellType, Sprite>();

    private Sprite m_CrossSprite;
    private Sprite m_CircleSprite;

    public Dictionary<CellType, Sprite> CellSpriteMap => m_CellSpriteMap;
    public event Action<PlayerType> OnGameCompleted;
    
    public void Initialize()
    {
        m_CrossSprite = GetSprite("Cross");
        m_CircleSprite = GetSprite("Circle");
        
        m_CellSpriteMap.Add(CellType.Cross, m_CrossSprite);
        m_CellSpriteMap.Add(CellType.Circle, m_CircleSprite);
        
        for (int i = 0; i < m_FieldModel.X; i++)
        {
            for (int j = 0; j < m_FieldModel.Y; j++)
            {
                var cell = m_Cells[i, j];
                
                cell.SetPositionInfo(i, j);
                
                cell.OnClickAction += MakeMove;
            }
        }
    }

    private void MakeMove(TicTacCell cell)
    {
        var currentCell = m_PlayerCellTypeMap[m_CurrentPlayer];
        cell.LockTap();
        cell.SetIcon(m_CellSpriteMap[currentCell]);
        cell.SetCellType(currentCell);

        if (CheckForWin())
        {
            OnGameCompleted?.Invoke(m_CurrentPlayer);
            LockAllCells();
        }
        else if (IsLastCellTaped())
        {
            OnGameCompleted?.Invoke(PlayerType.None);
        }
        
        SwitchPlayer();
        
        m_FieldPresenter.RegisterCell(cell);
    }

    private bool IsLastCellTaped()
    {
        foreach (var cell in m_Cells)
        {
            if (!cell.IsBlockedToTap)
            {
                return false;
            }
        }

        return true;
    }
    
    private void LockAllCells()
    {
        foreach (var cell in m_Cells)
        {
            cell.LockTap();
        }
    }
    
    private bool CheckForWin()
    {
        bool[] rows = { true, true, true };
        for (int i = 0; i < m_FieldModel.X; i++)
        {
            for (int j = 1; j < m_FieldModel.X; j++)
            {
                rows[i] &= m_Cells[i, j].CellType != CellType.None
                           && m_Cells[i, j].CellType == m_Cells[i, 0].CellType;
            }
        }
        
        bool[] cols = { true, true, true };
        for (int j = 0; j < m_FieldModel.X; j++)
        {
            for (int i = 1; i < m_FieldModel.X; i++)
            {
                cols[j] &= m_Cells[i, j].CellType != CellType.None
                           && m_Cells[i, j].CellType == m_Cells[0, j].CellType;
            }
        }

        bool mainDiagonal = m_Cells[0, 0].CellType != CellType.None &&
                            m_Cells[0, 0].CellType == m_Cells[1, 1].CellType &&
                            m_Cells[0, 0].CellType == m_Cells[2, 2].CellType;

        bool collateralDiagonal = m_Cells[2, 0].CellType != CellType.None &&
                                  m_Cells[2, 0].CellType == m_Cells[1, 1].CellType &&
                                  m_Cells[2, 0].CellType == m_Cells[0, 2].CellType;

        return rows.Any(x => x) || cols.Any(x => x) || mainDiagonal || collateralDiagonal;
    }
    
    private void SwitchPlayer()
    {
        m_CurrentPlayer = m_CurrentPlayer == PlayerType.One ? PlayerType.Two : PlayerType.One;
    }

    public void SetCurrentPlayerIfNeeded(PlayerType type)
    {
        m_CurrentPlayer = type;
    }
    
    private Sprite GetSprite(string key)
    {
        var asyncOperationHandler = m_AssetManager.LoadAsset<Sprite>(key);
        return asyncOperationHandler.Result;
    }

    public void Dispose()
    {
        for (int i = 0; i < m_FieldModel.X; i++)
        {
            for (int j = 0; j < m_FieldModel.Y; j++)
            {
                var cell = m_Cells[i, j];

                cell.OnClickAction -= MakeMove;
            }
        }
    }
}