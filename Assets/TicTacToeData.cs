﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

[Serializable, JsonObject(MemberSerialization.Fields)]
public class TicTacToeData
{
    [SerializeField] private List<TicTacCellModel> m_TicTacCellModels
        = new List<TicTacCellModel>();

    public List<TicTacCellModel> TicTacCellModels => m_TicTacCellModels;

    public void RegisterTicTacCellModel(TicTacCellModel model)
    {
        m_TicTacCellModels.Add(model);
    }

    public void ClearCellModels()
    {
        m_TicTacCellModels.Clear();
    }
}