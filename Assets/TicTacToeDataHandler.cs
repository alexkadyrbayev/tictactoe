using AlexKadyrbayev.Plugins.DataHandler;
using UnityEngine;

[CreateAssetMenu(menuName = "Dev/DataHandlers/TicTacToeDataHandler", fileName = "TicTacToeDataHandler")]
public class TicTacToeDataHandler : DataHandler<TicTacToeData> { }