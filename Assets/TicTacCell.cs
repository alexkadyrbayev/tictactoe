using System;
using UnityEngine;

public class TicTacCell : MonoBehaviour
{
    [SerializeField] private SpriteRenderer m_CellSprite;

    private PositionInfo m_PositionInfo;
    private CellType m_CellType;
    private bool m_IsBlockedToTap;

    public PositionInfo PositionInfo => m_PositionInfo;
    
    public Action<TicTacCell> OnClickAction;

    public CellType CellType => m_CellType;
    public bool IsBlockedToTap => m_IsBlockedToTap;
    
    private void OnMouseDown()
    {
        if (m_IsBlockedToTap)
        {
            return;
        }
        OnClickAction?.Invoke(this);
    }

    public void SetIcon(Sprite sprite)
    {
        m_CellSprite.sprite = sprite;
    }

    public void LockTap()
    {
        m_IsBlockedToTap = true;
    }

    public void SetCellType(CellType type)
    {
        m_CellType = type;
    }

    public void SetPositionInfo(int x, int y)
    {
        m_PositionInfo = new PositionInfo(x, y);
    }
}