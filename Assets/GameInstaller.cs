using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [SerializeField] private FieldView m_FieldView;
    [SerializeField] private TicTacCell m_TicTacCellPrefab;
    [SerializeField] private int m_FieldRowSize;
    [SerializeField] private int m_FieldColSize;
    
    public override void InstallBindings()
    {
        Container
            .Bind<IFieldPresenter>()
            .To<FieldPresenter>()
            .AsSingle()
            .NonLazy();
        
        var cells = CreateCells();
        Container
            .Bind<TicTacCell[,]>()
            .FromInstance(cells)
            .AsCached()
            .NonLazy();
        
        Container
            .Bind<IFieldModel>()
            .To<FieldModel>()
            .AsSingle()
            .WithArguments(
                m_FieldColSize,
                m_FieldRowSize)
            .NonLazy();
        
        Container
            .Bind<IFieldView>()
            .FromInstance(m_FieldView)
            .AsSingle()
            .NonLazy();

        Container
            .Bind<IPlayerInputController>()
            .To<PlayerInputController>()
            .AsSingle()
            .NonLazy();
    }

    private TicTacCell[,] CreateCells()
    {
        var cells = new TicTacCell[m_FieldRowSize, m_FieldColSize];
        for (int row = 0; row < m_FieldRowSize; row++)
        {
            for (int col = 0; col < m_FieldColSize; col++)
            {
                cells[row,col] = Container.
                    InstantiatePrefabForComponent<TicTacCell>(
                        m_TicTacCellPrefab);
            }
        }

        return cells;
    }
}
