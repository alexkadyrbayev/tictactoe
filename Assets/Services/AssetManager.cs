using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Services
{
    public class AssetManager : IAssetManager
    {
        public AsyncOperationHandle<T> LoadAsset<T>(object key)
        {
            var asyncOperationHandle = Addressables.LoadAssetAsync<T>(key);
            asyncOperationHandle.WaitForCompletion();
            return asyncOperationHandle;
        }

        public AsyncOperationHandle Instantinate<T>(object key, Transform parent, out T result)
        {
            var asyncOperationHandle = Addressables.InstantiateAsync(key, parent);
            result = asyncOperationHandle.Result.GetComponent<T>();
            asyncOperationHandle.WaitForCompletion();
            return asyncOperationHandle;
        }
    }
}