using Zenject;

namespace Services
{
    public class AssetManagerMonoInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container
                .BindInterfacesTo<AssetManager>()
                .AsSingle();
        }
    }
}