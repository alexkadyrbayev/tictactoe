using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;

namespace AlexKadyrbayev.Plugins.DataManagement
{
    public interface IReadOnlyJObjectData
    {
        /// <summary>
        /// ������� JToken �� �����
        /// </summary>
        /// <param name="dataKey"></param>
        /// <returns>JToken or null</returns>
        JToken GetJToken(string dataKey);

        /// <summary>
        /// �������� ������� ����� � ������
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool HasKey(string key);
    }

    /// <summary>
    /// CRUD ��� ������ � JObject
    /// </summary>
    public class JObjectData : IReadOnlyJObjectData
    {
        public JObject Data { get; private set; }

        public JObjectData()
        {
            Data = new JObject();
        }

        /// <summary>
        /// �������� ������ � ����������, ���� �� ��� ��� ���, �� ������� ����� 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        public void AddData(string key, JObject data)
        {
            if (HasKey(key))
            {
                Data[key] = data;
            }
            else
            {
                Data.Add(key, data);
            }
        }

        /// <summary>
        /// ������� JObject 
        /// </summary>
        /// <returns>JObject</returns>
        public JObject GetData()
        {
            return Data;
        }

        /// <summary>
        /// ������� JToken �� �����
        /// </summary>
        /// <param name="dataKey"></param>
        /// <returns>JToken or null</returns>
        public JToken GetJToken(string dataKey)
        {
            if (Data.TryGetValue(dataKey, out var jToken))
            {
                return jToken;
            }

            return null;
        }

        /// <summary>
        /// ������������ ��� ������ � ��������� ������ 
        /// </summary>
        /// <param name="data">JObject</param>
        public void AddAllData(JObject data)
        {
            Data = data ?? throw new ArgumentNullException("�� �� �������� ������ JObject ��� ������!");
        }


        /// <summary>
        /// ������� ������ �� ����� ���� ��� ����
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Remove(string key)
        {
            if (HasKey(key))
            {
                Data.Remove(key);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// �������� ������� ����� � ������
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool HasKey(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentException("�� ������ �������� ���� ��� ��������!");
            }

            return Data[key] != null;
        }
    }
}