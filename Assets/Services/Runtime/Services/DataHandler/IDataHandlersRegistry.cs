﻿using AlexKadyrbayev.Plugins.DataHandler;

namespace AlexKadyrbayev.Plugins.DataManagement
{
    public interface IDataHandlersRegistry
    {
        public IDataHandler GetDataHandlerObject(string dataKey);
        public IDataHandler<T> GetDataHandlerObject<T>(string dataKey);
    }
}