using Zenject;

namespace AlexKadyrbayev.Plugins.DataManagement
{
    public class DataHandlersRegistryInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container
                .BindInterfacesTo<DataHandlersRegistry>()
                .AsSingle()
                .NonLazy();
        }
    }
}