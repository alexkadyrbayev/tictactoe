using System.IO;
using UnityEngine;

namespace Services.AssetManagement
{
    public class JsonManager : IJsonManager
    {
        public T GetJson<T>(string json)
        {
            return JsonUtility.FromJson<T>(File.ReadAllText(json + ".json"));
        }
    }
}