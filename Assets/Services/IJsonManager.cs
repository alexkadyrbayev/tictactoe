namespace Services.AssetManagement
{
    public interface IJsonManager
    {
        public T GetJson<T>(string key);
    }
}