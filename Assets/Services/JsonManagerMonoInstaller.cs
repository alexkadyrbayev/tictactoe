using Zenject;

namespace Services.AssetManagement
{
    public class JsonManagerMonoInstaller : MonoInstaller<JsonManagerMonoInstaller>
    {
        public override void InstallBindings()
        {
            Container
                .Bind<IJsonManager>()
                .To<JsonManager>()
                .AsSingle()
                .NonLazy();
        }
    }
}