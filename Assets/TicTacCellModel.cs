using System;
using Newtonsoft.Json;
using UnityEngine;

[Serializable, JsonObject(MemberSerialization.Fields)]
public class TicTacCellModel
{
    [SerializeField] private PositionInfo m_PositionInfo;
    [SerializeField] private string m_OjbectName;

    public PositionInfo PositionInfo => m_PositionInfo;
    public string ObjectName => m_OjbectName;

    public TicTacCellModel(PositionInfo info, string objectName)
    {
        m_PositionInfo = info;
        m_OjbectName = objectName;
    }
}
