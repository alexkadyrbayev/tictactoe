using System;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerInputController
{
    public void Initialize();
    public event Action<PlayerType> OnGameCompleted;
    public void SetCurrentPlayerIfNeeded(PlayerType type);
    public Dictionary<CellType, Sprite> CellSpriteMap { get; }
}