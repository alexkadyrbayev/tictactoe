using UnityEngine;

public class FieldModel : IFieldModel
{
    [SerializeField] private int m_X;
    [SerializeField] private int m_Y;

    public int X => m_X;
    public int Y => m_Y;

    public FieldModel(int x, int y)
    {
        m_X = x;
        m_Y = y;
    }
}